const SerialPort = require("serialport");
const Readline = require("@serialport/parser-readline");
const exec = require("child_process").exec;
exec("ls /dev/tty.usbserial*", (e, stdout, stderr) => {
  if (e instanceof Error) {
    console.error(e);
    throw e;
  }
  const port = new SerialPort('/dev/tty.usbserial-14620', { baudRate: 9600 });
  const parser = port.pipe(new Readline({ delimiter: "\n" }));
  // Read the port data
  port.on("open", () => {
    console.log("serial port open");
  });
  parser.on("data", (data) => {
    console.log("got word from arduino:", data);
  });
});
